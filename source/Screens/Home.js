import React from 'react';
import { TouchableOpacity } from 'react-native';
import { SafeAreaView,  View, Image, FlatList, StyleSheet, Text, StatusBar } from 'react-native';

const DATA = [
  {
    title: 'Deals',
    img:    require('../../source/assets/deals.jpeg')
  },
  {
    title: 'Fashion',
    img:   require('../../source/assets/fashion.png')
  },
  {
    title: 'Bakery',
    img: require('../../source/assets/bakery.png')
  },
  {
    title: 'Fish & Chicken',
    img: require('../../source/assets/fish.jpg')
  },
  {
    title: 'Electrical & Electronics',
    img: require('../../source/assets/electrical.jpg')
  },
  {
    title: 'Fruits & Vegetables',
    img: require('../../source/assets/vegetable.png')
  },
 
  {
    title: 'Household',
    img: require('../../source/assets/house.png')
  },

  {
    title: 'Grocery',
    img: require('../../source/assets/grocery.jpg')
  },
  {
    title: 'Beauty',
    img: require('../../source/assets/beauty.png')
  },
];

const FlatListBasics = (props) => {
 
  const renderItem1 = ({ item }) => (
    <View style={styles.container}>
      <TouchableOpacity onPress= {()=>props.navigation.navigate('Category')}>
      <View style={styles.container1}   >
      <Image source= {item.img} style={styles.image} />
    <Text style={styles.title}>{item.title}</Text>
      </View>
      </TouchableOpacity>
  
  
 </View>
  );

  return (
    <SafeAreaView style={styles.container}>
  <Image source= { require('../../source/assets/ads.jpg')} style={styles.image1} />
      <FlatList
      horizontal={false}
        data={DATA}
        numColumns='3'
        renderItem={renderItem1}
        keyExtractor={item => item.title}
      />
      
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding:7,
    backgroundColor: '#bdbdbd'
  },
  container1: {
    flex: 1,
    padding: 5, 
    backgroundColor: 'white'
  },
  image1:{
width:350,
height:150,
  },
  image: {
    width: 90,
    height: 90,
    borderRadius: 150 / 2,
    overflow: "hidden",
alignItems: 'center',
justifyContent: 'center',
  },
  item: {
    backgroundColor: '#f9c2ff',
    padding: 20,
    marginVertical: 8,
    marginHorizontal: 16,
  },
  title: {
    fontSize: 14,
    fontStyle:'italic',
    alignContent: 'center',
    textAlign: 'center',
    flex: 1
  },
});

export default FlatListBasics;