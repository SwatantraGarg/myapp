import React, { Component } from "react";
import { StyleSheet, Text, TextInput, View } from "react-native";
import { Button } from "react-native-elements";
import LinearGradient from 'react-native-linear-gradient';

export default class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      hi: ''
    }
  }

  render() {
    
    const { hi } = this.state;
    return (
    
       <View style={{ flex: 1}}>

       <LinearGradient colors={['#c4c4c4', '#3b5998', 'pink']} style={styles.linearGradient}>
         <View>
           <Text dataDetectorType='phoneNumber' style={{ textAlign: 'center', padding: 40, fontWeight: "bold", fontSize: 20 }} >

             <Text style={{ textAlign: 'center', padding: 40, color: "red", fontSize: 20 }}>Login</Text>
           </Text>
           <TextInput style={Style.container}
             keyboardType='email-address' placeholder='Enter your email' ></TextInput>

           <TextInput style={Style.container}
             keyboardType='default' placeholder='Enter your password' secureTextEntry={true}></TextInput>
         </View>
         <Button containerStyle={{ margin: 30, backgroundColor: 'black' }} raised={true} title="Login" onPress={()=>this.props.navigation.navigate('Home')}></Button>

         <Text style={{padding: 15, color: 'white'}}>Don't have an acount?<Text style={{color:'orange', fontSize: 15}} onPress={()=>this.props.navigation.navigate('SignUp')}> create a new account</Text></Text>
       </LinearGradient>

     </View>
    );
  }
}
const Style = StyleSheet.create({
  container:
    { height: 40, borderColor: 'white', borderWidth: 1, padding: 10, margin: 30, backgroundColor: '#ffffff' }
})


var styles = StyleSheet.create({
  linearGradient: {
    flex: 1,
    paddingLeft: 15,
    paddingRight: 15,
    borderRadius: 5
  },
  buttonText: {
    fontSize: 18,
    fontFamily: 'Gill Sans',
    textAlign: 'center',
    margin: 10,
    color: '#ffffff',
    backgroundColor: 'transparent',
  },
})
