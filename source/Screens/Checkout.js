import React,{useState} from 'react';
import { SafeAreaView, View, FlatList, StyleSheet,  StatusBar } from 'react-native';
import { Image, Text, Button } from 'react-native-elements';
import Modal from 'react-native-modal';
const DATA = [
  {
    title: 'Kurti1',
    img:    require('../../source/assets/k1.jpg'),
    price: '₹1750'
  },
  {
    title: 'Kurti2',
    img:   require('../../source/assets/k2.jpg'),
    price: '₹2000'
  },
  {
    title: 'Kurti3',
    img: require('../../source/assets/k3.jpg'),
    price: '₹2500'
  },
  {
    title: 'Kurti4',
    img: require('../../source/assets/k4.jpg'),
    price: '₹800'
  },
  {
    title: 'kurti5',
    img: require('../../source/assets/k5.jpg'),
    price: '₹1000'
  },

];

const CheckoutFlatlist = () => {
 const [visible,setVisible]=useState(false)
  const renderItem1 = ({ item }) => (
    <View style={styles.container}>
      <View style={styles.container1}>
        <Image source={item.img} style={styles.image} />

        <Text style={styles.title}>{item.title}</Text>
        <Text style={styles.title}>{item.price}</Text>
       <View style={{width:100,height:50, borderRadius:15,flexDirection:'row', backgroundColor:'orange',margin:5}}>
         <Text style={{padding:7,fontSize:25}}>-</Text>
         <Text style={{padding:10,fontSize:20, marginLeft:10}}>1</Text>
          <Text style={{padding:7,fontSize:25, marginLeft:10}}>+</Text>
       </View>
  
      </View>

    </View>
  );

  return (
    <SafeAreaView style={styles.container}>
  
  
    <View style={{elevation:1,borderRadius:3, height:120, margin:10}}>
    <Text style={styles.title}>Shipping Address</Text>
      <Text style={styles.title}>21/35, taxmann publication, near agarsen hospital, 110021</Text>
   
    </View>
    <View style={{margin:10,elevation:1,borderRadius:3, height:120}}>
      <Text style={styles.title}>Billing Address</Text>
      <Text style={styles.title}>21/35, taxmann publication, near agarsen hospital, 110021</Text>
    </View>
    <FlatList
        horizontal={false}
        data={DATA}

        renderItem={renderItem1}
        keyExtractor={item => item.title}
      />
     <View  style={{height:50, borderRadius:10, backgroundColor:'orange', flexDirection:'row',justifyContent:'space-between'}}>
       <Text style={{padding:10, fontSize:18  }}>Total    ₹1000</Text>
       <Text style={{padding:10, fontSize:18, backgroundColor:'black', color: 'orange'}}onPress={()=>setVisible(true)}>Check Out</Text>
    
    
</View>
<Modal isVisible={visible} style={{flex: 1,maxHeight:100, marginTop:300,backgroundColor:'#fff',alignSelf:'center',alignContent:'center',justifyContent:'center', width:200}}>
        <View style={{justifyContent:'center'}}>
        <View style={{backgroundColor:'green', alignItems:'center'}}></View>
          <Text style={{padding:15}}>Congratulations! your order has been processed</Text>
          <Button title='OK' onPress={()=>setVisible(false)} />
        </View>
      </Modal>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding:3,
    backgroundColor: '#bebebe'
  },
  container1: {
    flex: 1,
    flexDirection: 'row',
    alignItems:'center',
    backgroundColor: 'white'
  },
  image1:{
width:45,
height:45,
alignItems:'center',
alignContent:'center'

  },
  image: {
    width: 90,
    height: 90,
   
alignItems: 'center',
justifyContent: 'center',
  },
  item: {
    backgroundColor: '#f9c2ff',
    padding: 20,
    marginVertical: 8,
    marginHorizontal: 16,
  },
  title: {
    fontSize: 16,
    fontStyle:'italic',
    backgroundColor:'white',
    alignContent: 'center',
    padding:10,
    color:'black',
    flex: 1
  },
});

export default CheckoutFlatlist;


