import React from 'react';
import { SafeAreaView, View, FlatList, StyleSheet, Text, StatusBar } from 'react-native';
import { Image } from 'react-native-elements'
const DATA = [
  {
    title: 'Kurti1',
    img: require('../../source/assets/k1.jpg'),
    price: '₹1750'
  },
  {
    title: 'Kurti2',
    img: require('../../source/assets/k2.jpg'),
    price: '₹2000'
  },
  {
    title: 'Kurti3',
    img: require('../../source/assets/k3.jpg'),
    price: '₹2500'
  },
  {
    title: 'Kurti4',
    img: require('../../source/assets/k4.jpg'),
    price: '₹800'
  },
  {
    title: 'kurti5',
    img: require('../../source/assets/k5.jpg'),
    price: '₹1000'
  },
  {
    title: 'Kurti6',
    img: require('../../source/assets/k6.jpg'),
    price: '₹1400'
  },
  {
    title: 'Kurti7',
    img: require('../../source/assets/k7.jpg'),
    price: '₹1200'
  },
  {
    title: 'Kurti8',
    img: require('../../source/assets/k8.jpg'),
    price: '₹200'
  },

];

const Category = (props) => {
  console.log(props);

  const renderItem1 = ({ item }) => (
    <View style={styles.container}>
      <View style={styles.container1}>
        <Image source={item.img} style={styles.image} />

        <Text style={styles.title}>{item.title}</Text>
        <Text style={styles.title}>{item.price}</Text>
        <Image source={require('../../source/assets/atc.jpg')} onPress={() => props.navigation.navigate('Checkout')} style={styles.image1}></Image>
      </View>

    </View>
  );


  return (
    <SafeAreaView style={styles.container}>

      <FlatList
        horizontal={false}
        data={DATA}

        renderItem={renderItem1}
        keyExtractor={item => item.title}
      />

    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 7,
    backgroundColor: '#bdbdbd'
  },
  container1: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    padding: 5,
    backgroundColor: 'white'
  },
  image1: {
    width: 45,
    height: 45,
    alignItems: 'center',
    alignContent: 'center'

  },
  image: {
    width: 90,
    height: 90,

    alignItems: 'center',
    justifyContent: 'center',
  },
  item: {
    backgroundColor: '#f9c2ff',
    padding: 20,
    marginVertical: 8,
    marginHorizontal: 16,
  },
  title: {
    fontSize: 16,
    fontStyle: 'italic',
    alignContent: 'center',
    textAlign: 'center',
    color: 'orange',
    flex: 1
  },
});

export default Category;


